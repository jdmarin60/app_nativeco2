﻿using System.Collections.Generic;

namespace App_NativeCO2
{
    public class G1
    {
        public string Color { get; set; }
        public string Label { get; set; }
        public int Valor { get; set; }
        public string ValueLabel { get; set; }
    }

    public class G2
    {
        public string Color { get; set; }
        public string Label { get; set; }
        public int Valor { get; set; }
        public string ValueLabel { get; set; }
    }

    public class G3
    {
        public string Color { get; set; }
        public string Label { get; set; }
        public int Valor { get; set; }
        public string ValueLabel { get; set; }
    }

    public class RootObject
    {
        public List<G1> G1 { get; set; }
        public List<G2> G2 { get; set; }
        public List<G3> G3 { get; set; }
    }
}
