﻿using Plugin.Connectivity;
using System.Threading.Tasks;

namespace App_NativeCO2
{
    class ValidateConex
    {
        public async Task<bool> validar()
        {
            CrossConnectivity.Current.Dispose();
            var connectivity = CrossConnectivity.Current;
            
            if (!connectivity.IsConnected)
                return false;

            var reachable = await connectivity.IsReachable("google.com");

            return true;
        }

    }
}
