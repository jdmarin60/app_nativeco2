﻿namespace App_NativeCO2.Views
{
    using System;
    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;


    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NoticiasGlobal : ContentPage
    {

        public NoticiasGlobal()
        {
            InitializeComponent();
        }

        private void OnBack(object sender, EventArgs e)
        {
            webView.GoBack();
        }

        private void OnReload(object sender, EventArgs e)
        {
            webView.Reload();
        }
    }
}
    