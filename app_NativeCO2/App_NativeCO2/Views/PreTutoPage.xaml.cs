﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App_NativeCO2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PreTutoPage : ContentPage
	{
		public PreTutoPage ()
		{
			InitializeComponent ();
		}

        private void Omitir_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new MenuPage());
        }
    }
}