﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App_NativeCO2.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NoticiasSalud : ContentPage
	{
		public NoticiasSalud ()
		{
			InitializeComponent ();
		}

        private void OnBack(object sender, EventArgs e)
        {
            webView.GoBack();
        }

        private void OnReload(object sender, EventArgs e)
        {
            webView.Reload();
        }
    }
}