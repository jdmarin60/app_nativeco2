﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App_NativeCO2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CalculadoraPage : ContentPage
    {
        const int grProteinBovine = 26;
        const int grProteinChicken = 27;
        const int grProteinPork = 27;
        const int grCO2Bovine = 295;
        const int grCO2Chicken = 35;
        const int grCO2Pork = 55;
        double meatd;
        double result;
        double value;

        double totalgrProtein;

        String op;
        String meat;
        String gramsBovine = grProteinBovine.ToString();
        String gramsChicken = grProteinChicken.ToString();
        String gramsPork = grProteinPork.ToString();
        String gramsCO2Bovine = grCO2Bovine.ToString();
        String gramsco2Chicken = grCO2Chicken.ToString();
        String gramsCO2Pork = grCO2Pork.ToString();


        public CalculadoraPage()
        {
            InitializeComponent();
            pkMeat.Items.Add("Vacuno");
            pkMeat.Items.Add("Cerdo");
            pkMeat.Items.Add("Pollo");
            obtenerProductos();
        }

        public void obtenerProductos()
        {
            HttpClient Client = new HttpClient();
            var  response = Client.GetStringAsync("https://app-co2off.firebaseio.com/Productos.json").Result;
            response = response.Replace("null,",string.Empty);
            var SerialResponse = JsonConvert.DeserializeObject<List<Producto>>(response);
            listView.ItemsSource = SerialResponse;
        }

        private void PkSelectMeats(object sender, EventArgs e)
        {
            op = pkMeat.SelectedItem.ToString();
            lbl_meat.Text = op;

            if (op == "Vacuno")
            {
                lbl_meat_grams.Text = gramsBovine;
            }

            if (op == "Cerdo")
            {
                lbl_meat_grams.Text = gramsPork;
            }

            if (op == "Pollo")
            {
                lbl_meat_grams.Text = gramsChicken;
            }
        }

        private void Calculate(object sender, EventArgs e)
        {
            meatd = 0;
            meat = null;

            if (String.IsNullOrEmpty(meatPounds.Text))
            {
                DisplayAlert("Cuidado", "Debes ingresar todos los campos", "OK");
            }

            else
            {
                meat = meatPounds.Text;
                Double.TryParse(meat, out value);

                if (value == 0)
                {
                    DisplayAlert("Cuidado", "Debes ingresar campos numericos", "OK");
                }

                else
                {
                    meatd = Double.Parse(meat);
                    if (op == "Vacuno")
                    {
                        this.result = (meatd / 100) * (grProteinBovine * grCO2Bovine);
                        this.result = this.result / 1000;
                        lbl_result.Text = this.result.ToString();
                        this.totalgrProtein = (meatd / 100) * grProteinBovine;
                        lbl_meat_grams1.Text = this.totalgrProtein.ToString();
                    }

                    if (op == "Cerdo")
                    {
                        this.result = (meatd / 100) * (grProteinPork * grCO2Pork);
                        this.result = this.result / 1000;
                        lbl_result.Text = this.result.ToString();
                        this.totalgrProtein = (meatd / 100) * grProteinPork;
                        lbl_meat_grams1.Text = this.totalgrProtein.ToString();
                    }

                    if (op == "Pollo")
                    {
                        this.result = (meatd / 100) * (grProteinChicken * grCO2Chicken);
                        this.result = this.result / 1000;
                        lbl_result.Text = this.result.ToString();
                        this.totalgrProtein = (meatd / 100) * grProteinChicken;
                        lbl_meat_grams1.Text = this.totalgrProtein.ToString();
                    }
                }    
            }
        }
    }
}