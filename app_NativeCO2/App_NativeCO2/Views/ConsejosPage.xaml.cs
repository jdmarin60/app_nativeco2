﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App_NativeCO2.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ConsejosPage : ContentPage
	{
		public ConsejosPage ()
		{
			InitializeComponent ();
		}
        
        private void OnReload(object sender, EventArgs e)
        {
            webView.Reload();
        }
    }
}