﻿using Microcharts;
using Newtonsoft.Json;
using SkiaSharp;
using System.Collections.Generic;
using System.Net.Http;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App_NativeCO2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EstadiPage : ContentPage
    {
        public EstadiPage()
        {
            InitializeComponent();
            ObtenerDatos();
        }

        public void ObtenerDatos()
        {
            HttpClient Client = new HttpClient();
            //Grafica1
            var response1 = Client.GetStringAsync("https://app-co2off.firebaseio.com/Estadisticas/G1.json").Result;
            response1 = response1.Replace("null,", string.Empty);
            var SerialResponse1 = JsonConvert.DeserializeObject<List<G1>>(response1);
            //Grafica2
            var response2 = Client.GetStringAsync("https://app-co2off.firebaseio.com/Estadisticas/G2.json").Result;
            response2 = response2.Replace("null,", string.Empty);
            var SerialResponse2 = JsonConvert.DeserializeObject<List<G2>>(response2);
            //Grafica3
            var response3 = Client.GetStringAsync("https://app-co2off.firebaseio.com/Estadisticas/G3.json").Result;
            response3 = response3.Replace("null,", string.Empty);
            var SerialResponse3 = JsonConvert.DeserializeObject<List<G3>>(response3);
            
            //Variables para Entradas
            var entries1 = new Microcharts.Entry[SerialResponse1.Count];
            var entries2 = new Microcharts.Entry[SerialResponse2.Count];
            var entries3 = new Microcharts.Entry[SerialResponse3.Count];

            //Valores Grafica 1
            for (int i = 0; i < SerialResponse1.Count; i++)
            {
                int valor = SerialResponse1[i].Valor;
                string label = SerialResponse1[i].Label;
                string color = SerialResponse1[i].Color;
                string valuelabel = SerialResponse1[i].ValueLabel;

                var entry1 = new Microcharts.Entry(valor)
                {
                    Label = label,
                    ValueLabel = valuelabel,
                    Color = SKColor.Parse(color)

                };

                entries1[i] = entry1;
            }

            var chart = new DonutChart()
            {
                Entries = entries1,
                LabelTextSize = 20,
                BackgroundColor = SKColors.Transparent
            };
            chartView1.Chart = chart;

            //Valores Grafica 2
            for (int i = 0; i < SerialResponse2.Count; i++)
            {
                int valor = SerialResponse2[i].Valor;
                string label = SerialResponse2[i].Label;
                string color = SerialResponse2[i].Color;
                string valuelabel = SerialResponse2[i].ValueLabel;

                var entry2 = new Microcharts.Entry(valor)
                {
                    Label = label,
                    ValueLabel = valuelabel,
                    Color = SKColor.Parse(color)

                };

                entries2[i] = entry2;
            }
            var chart2 = new LineChart()
            {
                Entries = entries2,
                LabelTextSize = 20,
                BackgroundColor = SKColors.Transparent,
                LineMode = LineMode.Straight
            };
            chartView2.Chart = chart2;

            //Valores Grafica 3
            for (int i = 0; i < SerialResponse3.Count; i++)
            {
                int valor = SerialResponse3[i].Valor;
                string label = SerialResponse3[i].Label;
                string color = SerialResponse3[i].Color;
                string valuelabel = SerialResponse3[i].ValueLabel;

                var entry3 = new Microcharts.Entry(valor)
                {
                    Label = label,
                    ValueLabel = valuelabel,
                    Color = SKColor.Parse(color)

                };

                entries3[i] = entry3;
            }
            var chart3 = new BarChart()
            {
                Entries = entries3,
                LabelTextSize = 18,
                BackgroundColor = SKColors.Transparent
            };
            chartView3.Chart = chart3;
        }
    }
}
