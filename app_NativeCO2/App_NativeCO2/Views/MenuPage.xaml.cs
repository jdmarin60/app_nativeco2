﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App_NativeCO2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MenuPage : ContentPage
	{
        ValidateConex connect = new ValidateConex();
        public MenuPage ()
		{
            InitializeComponent ();
            
		}


        private async void Button_Clicked(object sender, EventArgs e)
        {
            var validate = connect.validar();
            if (validate==true)
            {
                await Navigation.PushAsync(new ConsejosPage());
            }
            else
            {
                await DisplayAlert("Atencion", "Revisa tu conexión a Internet", "Yes");
            }
        }

        private async void Button_Clicked_1(object sender, EventArgs e)
        {
            var validate = connect.validar();
            if (validate == true)
            {
                await Navigation.PushAsync(new NoticiasPage());
            }
            else
            {
                await DisplayAlert("Atencion", "Revisa tu conexión a Internet", "Yes");
            }
        }

        private async void Button_Clicked_2(object sender, EventArgs e)
        {
            var validate = connect.validar();
            if (validate == true)
            {
                await Navigation.PushAsync(new CalculadoraPage());
            }
            else
            {
                await DisplayAlert("Atencion", "Revisa tu conexión a Internet", "Yes");
            }
        }

        private void Button_Clicked_4(object sender, EventArgs e)
        {
            Navigation.PushAsync(new CO2Page());
        }

        private async void Button_Clicked_5(object sender, EventArgs e)
        {
            var validate = connect.validar();
            if (validate == true)
            {
                await Navigation.PushAsync(new EstadiPage());
            }
            else
            {
                await DisplayAlert("Atencion", "Revisa tu conexión a Internet", "Yes");
            }
        }

        private void Info_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AcercadePage());
        }

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread( async () => 
            {
                var result = await this.DisplayAlert("Atencion","Realmente deseas salir de la app","Yes","No");
                if (result)
                {
                    System.Environment.Exit(0);
                } 
            });
            return true;
        }
    }
}