﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;

namespace App_NativeCO2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TutorialCalculadoraPage : ContentPage
	{
		public TutorialCalculadoraPage ()
		{
			InitializeComponent ();
		}

        private void Finalizar_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new MenuPage());
            Preferences.Set("finishtuto", "true");

        }
    }
}