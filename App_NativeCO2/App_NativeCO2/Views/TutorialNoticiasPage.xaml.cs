﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App_NativeCO2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TutorialNoticiasPage : ContentPage
    {
        public TutorialNoticiasPage ()
		{
			InitializeComponent ();
           
		}

        public void Omitir_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new MenuPage());
        
        }
    }


}