﻿using System.Collections.Generic;

namespace App_NativeCO2
{
    public class Producto
    {
        public string Detail { get; set; }
        public string Image { get; set; }
        public string Name { get; set; }
    }

    public class ListaProductos
    {
        public List<Producto> Productos { get; set; }
    }

}
