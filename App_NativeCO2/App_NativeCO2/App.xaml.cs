﻿namespace App_NativeCO2
{
    using Xamarin.Forms;
    using Xamarin.Essentials;
    using Views;

    public partial class App : Application
    {
        #region Constructors

        public App()
        {
            var myValue = Preferences.Get("finishtuto", "false");
            InitializeComponent();



            if (myValue !="true") {
            CarouselPage carouselPage = new CarouselPage();
            carouselPage.Children.Add(new PreTutoPage());
            carouselPage.Children.Add(new TutoEstadiPage());
            carouselPage.Children.Add(new TutorialConsejosPage());
            carouselPage.Children.Add(new TutorialNoticiasPage());
            carouselPage.Children.Add(new TutorialCalculadoraPage());

                
                MainPage = new NavigationPage(carouselPage)
                {
                    BarBackgroundColor = Color.FromHex("#121f1f"),
            };
                
            }
            else
            {
                MainPage = new NavigationPage(new MenuPage());
              
            }
        }
        #endregion

        #region Methods
        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        } 
        #endregion
    }
}
